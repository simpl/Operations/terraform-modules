resource "ovh_cloud_project_database_postgresql_connection_pool" "this" {
  for_each     = var.database-postgresql-connection-pool
  service_name = ovh_cloud_project_database.this[each.value["database-key"]].service_name
  cluster_id   = ovh_cloud_project_database.this[each.value["database-key"]].id
  database_id  = ovh_cloud_project_database_database.this[each.value["database-database-key"]].id
  user_id      = ovh_cloud_project_database_postgresql_user.this[each.value["database-postgresql-user-key"]].id
  name         = each.value["database-postgresql-connection-pool-name"]
  mode         = each.value["database-postgresql-connection-pool-mode"]
  size         = each.value["database-postgresql-connection-pool-size"]
}
