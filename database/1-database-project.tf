resource "ovh_cloud_project_database" "this" {
  for_each     = var.database-project
  service_name = each.value["database-project-service-name"]
  description  = each.value["database-project-description"]
  engine       = each.value["database-project-engine"]
  version      = each.value["database-project-version"]
  plan         = each.value["database-project-plan"]
  flavor       = each.value["database-project-flavor"]

  dynamic "nodes" {
    for_each = var.custom-nodes
    content {
      region     = nodes.value["database-project-nodes-region"]
      network_id = nodes.value["database-project-nodes-network-id"]
      subnet_id  = nodes.value["database-project-nodes-subnet-id"]
    }
  }

  dynamic "ip_restrictions" {
    for_each = each.value["custom-ip-restriction"]
    content {
      description = ip_restrictions.value["database-ip-restriction-description"]
      ip          = ip_restrictions.value["database-ip-restriction-ip"]
    }
  }
}

