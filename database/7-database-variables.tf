variable "database-project" {
  description = "map variable for helm releases"
  type = map(object({
    database-project-service-name = string
    database-project-description  = string
    database-project-engine       = string
    database-project-version      = string
    database-project-plan         = string
    database-project-flavor       = string
    custom-ip-restriction = map(object({
      database-ip-restriction-description = string
      database-ip-restriction-ip          = string
    }))
  }))
}

variable "database-postgresql-connection-pool" {
  description = "connection pool for user to connect to database"
  type = map(object({
    database-key                             = string
    database-database-key                    = string
    database-postgresql-user-key             = string
    database-postgresql-connection-pool-size = number
    database-postgresql-connection-pool-name = string
    database-postgresql-connection-pool-mode = string
  }))
}

variable "database-database" {
  description = "instances of databases in database"
  type = map(object({
    database-database-name = string
    database-key           = string
  }))
}

variable "database-postgresql-users" {
  description = "map of users created in postgresql database"
  type = map(object({
    database-postgresql-user-name  = string
    database-postgresql-user-roles = list(string)
    database-key                   = string
  }))
}

variable "custom-nodes" {
  description = "custom nodes variable for database config"
  type = map(object({
    database-project-nodes-subnet-id  = string
    database-project-nodes-network-id = string
    database-project-nodes-region     = string
  }))
}

