resource "ovh_cloud_project_database_postgresql_user" "this" {
  for_each     = var.database-postgresql-users
  name         = each.value["database-postgresql-user-name"]
  cluster_id   = ovh_cloud_project_database.this[each.value["database-key"]].id
  service_name = ovh_cloud_project_database.this[each.value["database-key"]].service_name
  roles        = each.value["database-postgresql-user-roles"]
  depends_on   = [ovh_cloud_project_database.this]
}

