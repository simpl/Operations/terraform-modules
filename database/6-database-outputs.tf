output "database-postgresql-user-password-output" {
  value      = [for user in ovh_cloud_project_database_postgresql_user.this : user.password]
  sensitive  = true
  depends_on = [ovh_cloud_project_database.this, ovh_cloud_project_database_database.this, ovh_cloud_project_database_postgresql_user.this]
}

output "database-postgresql-user-name-output" {
  value      = [for user in ovh_cloud_project_database_postgresql_user.this : user.name]
  depends_on = [ovh_cloud_project_database.this, ovh_cloud_project_database_database.this, ovh_cloud_project_database_postgresql_user.this]
}

output "database-database-name-output" {
  value      = [for database in ovh_cloud_project_database_database.this : database.name]
  depends_on = [ovh_cloud_project_database.this, ovh_cloud_project_database_database.this, ovh_cloud_project_database_postgresql_user.this]
}

output "database-project-endpoints-domain" {
  value = [for database_project in ovh_cloud_project_database.this : database_project.endpoints[0].domain]
}

output "database-project-endpoints-port" {
  value = [for database_project in ovh_cloud_project_database.this : database_project.endpoints[0].port]
}

output "database-project-endpoints-scheme" {
  value = [for database_project in ovh_cloud_project_database.this : database_project.endpoints[0].scheme]
}
