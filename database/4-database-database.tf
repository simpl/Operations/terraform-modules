resource "ovh_cloud_project_database_database" "this" {
  for_each     = var.database-database
  service_name = ovh_cloud_project_database.this[each.value["database-key"]].service_name
  engine       = ovh_cloud_project_database.this[each.value["database-key"]].engine
  cluster_id   = ovh_cloud_project_database.this[each.value["database-key"]].id
  name         = each.value["database-database-name"]
}
