resource "ovh_cloud_project_gateway" "this" {
  service_name = var.service-name
  name         = var.gateway-name
  model        = var.gateway-model
  region       = var.gateway-region
  network_id   = var.gateway-network-private-openstackid
  subnet_id    = var.gateway-network-private-subnet-id
}
