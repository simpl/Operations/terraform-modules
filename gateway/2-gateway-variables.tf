variable "service-name" {
  description = "Project ID"
  type        = string
}

variable "gateway-name" {
  description = "Name of the gateway"
  type        = string
}

variable "gateway-model" {
  description = "Model of the gateway"
  type        = string
}

variable "gateway-region" {
  description = "Gateway region as an ouput of network region"
  type        = string
}

variable "gateway-network-private-openstackid" {
  description = "Gateway region as an output of network private openstackid"
  type        = string
}

variable "gateway-network-private-subnet-id" {
  description = "Output of network subnet id"
  type        = string
}
