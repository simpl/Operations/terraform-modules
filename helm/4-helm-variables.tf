variable "helm-host" {
  description = "host of k8s cluster"
  type        = string
}

variable "helm-client-certificate" {
  description = "k8s cluster client certificate"
  type        = string
}

variable "helm-client-key" {
  description = "k8s cluster client key"
  type        = string
}

variable "helm-client-ca-certificate" {
  description = "k8s cluster ca-certificate"
  type        = string
}

variable "helm-release" {
  description = "map variable for helm releases"
  type = map(object({
    helm-release-create-namespace    = bool
    helm-release-namespace           = string
    helm-release-version             = string
    helm-release-chart               = string
    helm-release-repository          = string
    helm-release-name                = string
    helm-release-custom-config-files = list(string)
    helm-release-timeout = number
    custom-sets = map(object({
      set-name  = string
      set-value = string
    }))
  }))
}
