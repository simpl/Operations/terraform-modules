resource "helm_release" "this" {
  for_each         = var.helm-release
  name             = each.value["helm-release-name"]
  repository       = each.value["helm-release-repository"]
  chart            = each.value["helm-release-chart"]
  version          = each.value["helm-release-version"]
  namespace        = each.value["helm-release-namespace"]
  create_namespace = each.value["helm-release-create-namespace"]
  values           = each.value["helm-release-custom-config-files"]
  timeout           = each.value["helm-release-timeout"]

  dynamic "set" {
    for_each = each.value["custom-sets"]
    content {
      name  = set.value["set-name"]
      value = set.value["set-value"]
    }
  }
}

