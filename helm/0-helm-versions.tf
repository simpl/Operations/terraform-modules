terraform {
  required_version = ">= 1.0"
  required_providers {
    helm = {
      source  = "hashicorp/helm"
      version = "~>2.0"
    }
    ovh = {
      source  = "ovh/ovh"
      version = "~>0.40"
    }
  }
}
