provider "helm" {
  kubernetes {
    host                   = var.helm-host
    client_certificate     = var.helm-client-certificate
    client_key             = var.helm-client-key
    cluster_ca_certificate = var.helm-client-ca-certificate
  }
}
