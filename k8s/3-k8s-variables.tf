# Managed Kubenetes Service

variable "k8s-project-nodepool-autoscale" {
  description = "switch for autoscale feature"
  type        = bool
}

variable "k8s-project-nodepool-flavor-name" {
  description = "Name for the nodepool flavor"
  type        = string
}

variable "k8s-project-nodepool-desired-nodes" {
  description = "Number of desired nodes"
  type        = number
}

variable "k8s-project-nodepool-max-nodes" {
  description = "Number of max nodes"
  type        = number
}

variable "k8s-project-nodepool-min-nodes" {
  description = "Number of min nodes"
  type        = number
}

variable "k8s-project-name" {
  description = "Name for the project"
  type        = string
}

variable "k8s-project-region" {
  description = "Name of the k8s region"
  type        = string
}

variable "k8s-project-version" {
  description = "Version of deployed k8s"
  type        = string 
}

variable "k8s-project-network-openstackid" {
  description = "Network openstack ID for this k8s instance"
  type        = string
}

variable "k8s-project-default-vrack-gateway" {
  description = "egress traffic will be routed towards this IP address"
  type        = string
}

variable "k8s-project-private_network_routing_as_default" {
  description = "Defines whether routing should default to using the nodes' private interface, instead of their public interface"
  type        = bool
}

variable "k8s-project-nodepool-name" {
  description = "Name for k8s nodepool"
  type        = string
}

variable "service-name" {
  description = "project service name"
  type        = string
}
