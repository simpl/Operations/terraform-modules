output "k8s-kubeconfig" {
  value     = ovh_cloud_project_kube.this.kubeconfig
  sensitive = true
}

output "k8s-host" {
  value     = ovh_cloud_project_kube.this.kubeconfig_attributes[0].host
  sensitive = true
}

output "k8s-client-certificate" {
  value     = ovh_cloud_project_kube.this.kubeconfig_attributes[0].client_certificate
  sensitive = true
}

output "k8s-client-key" {
  value     = ovh_cloud_project_kube.this.kubeconfig_attributes[0].client_key
  sensitive = true
}

output "k8s-client-ca-certificate" {
  value     = ovh_cloud_project_kube.this.kubeconfig_attributes[0].cluster_ca_certificate
  sensitive = true
}
