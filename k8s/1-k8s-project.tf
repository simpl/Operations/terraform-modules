resource "ovh_cloud_project_kube" "this" {
  service_name       = var.service-name
  name               = var.k8s-project-name
  region             = var.k8s-project-region
  private_network_id = var.k8s-project-network-openstackid
  version = var.k8s-project-version

  private_network_configuration {
    default_vrack_gateway              = var.k8s-project-default-vrack-gateway
    private_network_routing_as_default = var.k8s-project-private_network_routing_as_default
  }
}

