resource "ovh_cloud_project_kube_nodepool" "this" {
  service_name  = var.service-name
  kube_id       = ovh_cloud_project_kube.this.id
  name          = var.k8s-project-nodepool-name
  flavor_name   = var.k8s-project-nodepool-flavor-name
  desired_nodes = var.k8s-project-nodepool-desired-nodes
  max_nodes     = var.k8s-project-nodepool-max-nodes
  min_nodes     = var.k8s-project-nodepool-min-nodes
  autoscale     = var.k8s-project-nodepool-autoscale
  depends_on    = [ovh_cloud_project_kube.this]
}
