resource "ovh_cloud_project_network_private" "this" {
  service_name = var.service-name
  name         = var.network-private-name # Network name
  regions      = var.network-private-regions
  vlan_id      = var.network-private-vlan-id # VLAN ID for vRack
}
