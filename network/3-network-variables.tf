variable "service-name" {
  description = "Project ID"
  type        = string
}

variable "network-private-name" {
  description = "private network name"
  type        = string
}

variable "network-private-region" {
  description = "list of regions for private network"
  type        = string
}

variable "network-private-regions" {
  description = "list of regions for private network"
  type        = list(string)
}

variable "network-private-vlan-id" {
  description = "vlan id"
  type        = number
}

variable "network-private-subnet-start" {
  description = "start of the ip range for hosts"
  type        = string
}

variable "network-private-subnet-end" {
  description = "end of the ip range for the hosts"
  type        = string
}

variable "network-private-subnet" {
  description = "network subnet with mask"
  type        = string
}

variable "network-private-subnet-dhcp" {
  description = "enable DHCP"
  type        = bool
}

variable "network-private-subnet-no-gateway" {
  description = "is default gatweay used?"
  type        = bool
}

