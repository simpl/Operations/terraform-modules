output "network-private-openstackid-out" {
  value = one(ovh_cloud_project_network_private.this.regions_attributes[*].openstackid)
}

output "network-private-subnet-id-out" {
  value = ovh_cloud_project_network_private_subnet.this.id
}

