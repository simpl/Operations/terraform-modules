resource "ovh_cloud_project_network_private_subnet" "this" {
  service_name = var.service-name
  network_id   = ovh_cloud_project_network_private.this.id
  start        = var.network-private-subnet-start # First IP of the subnet
  end          = var.network-private-subnet-end   # Last IP of the subnet
  network      = var.network-private-subnet       # Subnet IP address location
  dhcp         = var.network-private-subnet-dhcp  # Enables DHCP
  region       = var.network-private-region
  no_gateway   = var.network-private-subnet-no-gateway # No default gateway
  depends_on   = [ovh_cloud_project_network_private.this]
}
