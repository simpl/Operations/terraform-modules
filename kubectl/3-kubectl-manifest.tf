data "kubectl_path_documents" "this" {
  pattern = "${path.module}/${var.kubectl-manifest-config-path}"
}

resource "kubectl_manifest" "this" {
  for_each  = toset(data.kubectl_path_documents.this.documents)
  yaml_body = each.value
}
