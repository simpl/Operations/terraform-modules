provider "kubectl" {
  host                   = var.kubectl-host
  client_certificate     = var.kubectl-client-certificate
  client_key             = var.kubectl-client-key
  cluster_ca_certificate = var.kubectl-cluster-ca-certificate
  load_config_file       = false
}
