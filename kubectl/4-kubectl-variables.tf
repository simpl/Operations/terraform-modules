variable "kubectl-manifest-config-path" {
  description = "Path to custom yaml kubernetes config file"
  type        = string
}

variable "kubectl-host" {
  description = "host of k8s cluster"
  type        = string
}

variable "kubectl-client-certificate" {
  description = "k8s cluster client certificate"
  type        = string
}

variable "kubectl-client-key" {
  description = "k8s cluster client key"
  type        = string
}

variable "kubectl-cluster-ca-certificate" {
  description = "k8s cluster ca-certificate"
  type        = string
}
